module.exports = function () {
  /* CSS */
  require('bootstrap/dist/css/bootstrap.min.css');
  require('font-awesome/css/font-awesome.css');
  require('./assets/css/app.css');
  
  /* JS */
  global.$ = global.jQuery = require('jquery');
  require('bootstrap/dist/js/bootstrap.min.js');
  require('angular');
  require('angular-route/angular-route.min.js');
  require('angular-resource/angular-resource.min.js');
  require('./assets/js/common.js');
};