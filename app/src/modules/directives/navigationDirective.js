
angular.module('SearchApp').directive('tableNavigation', function($document, $interval) {
  return {
    restrict: 'A',
    link : function(scope, element) {
      $document.bind('keydown', navigation);
  
      element.on('$destroy', function() {
        $document.unbind('keydown', navigation);
      });
  
      function navigation(e) {
        
        var code = e.keyCode || e.which;
        var new_elm;
        var new_td_index;
    
        if(!scope.navigation.elm && [40,38,39,37].indexOf(code) !== -1) {
          e.preventDefault();
      
          scope.navigation.td_count = $('.search__table-navigation__directive:first').closest('tr').find('td').length;
          scope.navigation.td_index = 0;
          scope.navigation.elm = $('.search__table-navigation__directive:eq('+ scope.navigation.td +')');
          scope.navigation.elm.focus();
          scope.$apply();
      
          return false;
        }
    
        switch (code) {
          // Escape
          case 27:
            scope.quickQuestionTable.show = false;
            scope.$apply();
        
            break;
      
          // arrow down
          case 40:
            e.preventDefault();
            new_elm = scope.navigation.elm.closest('tr').next().find('td .search__table-navigation__directive:eq('+ scope.navigation.td_index +')');
        
            if(new_elm.length !== 0) {
              scope.navigation.elm = new_elm;
              scope.navigation.elm.focus();
              scope.$apply();
            }
        
            break;
      
          // arrow up
          case 38:
            e.preventDefault();
            new_elm = scope.navigation.elm.closest('tr').prev().find('td .search__table-navigation__directive:eq('+ scope.navigation.td_index +')');
        
            if(new_elm.length !== 0) {
              scope.navigation.elm = new_elm;
              scope.navigation.elm.focus();
              scope.$apply();
            }
        
            break;
      
          // arrow right
          case 39:
            e.preventDefault();
            new_td_index = scope.navigation.td_index + 1;
            new_elm = scope.navigation.elm.closest('tr').find('td .search__table-navigation__directive:eq('+ new_td_index +')');
        
            if(new_elm.length !== 0) {
              scope.navigation.td_index = new_td_index;
              scope.navigation.elm = new_elm;
              scope.navigation.elm.focus();
              scope.$apply();
            }
        
            break;
      
          // arrow left
          case 37:
            e.preventDefault();
            new_td_index = scope.navigation.td_index > 0 ? scope.navigation.td_index - 1 : 0;
            new_elm = scope.navigation.elm.closest('tr').find('td .search__table-navigation__directive:eq('+ new_td_index +')');
        
            if(new_elm.length !== 0) {
              scope.navigation.td_index = new_td_index;
              scope.navigation.elm = new_elm;
              scope.navigation.elm.focus();
              scope.$apply();
            }
        
            break;
      
          // Enter
          case 13:
            e.preventDefault();
            
            var click_elm = scope.navigation.elm.find('a');
  
            if(click_elm.length !== 0) {
              click_elm.trigger('click');
            }
        
            break;
        }
      }
    }
  };
});


