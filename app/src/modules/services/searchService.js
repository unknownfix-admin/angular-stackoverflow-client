angular.module('SearchApp')
  .factory('search', function ($resource, appConfig) {
    return $resource(appConfig.api_url + '2.2/search?order=desc&sort=activity&site=stackoverflow', {}, {
      query: {method: "GET", isArray: false}
    });
  })
  .factory('searchByTag', function ($resource, appConfig) {
  return $resource(appConfig.api_url + '2.2/search?order=desc&sort=activity&site=stackoverflow&page=1&pagesize=5&tagged=:tag',
    {tag: '@tag'}, 
    {query: {method: "GET", isArray: false}
  });
});