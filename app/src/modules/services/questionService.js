angular.module('SearchApp')
  .factory('question', function ($resource, appConfig) {
    return $resource(appConfig.api_url + '/2.2/questions/:id?order=desc&sort=activity&site=stackoverflow&filter=!9YdnSIN*R',
      {id: '@id'},
      {query: {method: "GET", isArray: false}
    });
  })
  .factory('questionAnswers', function ($resource, appConfig) {
  return $resource(appConfig.api_url + '/2.2/questions/:id/answers?order=desc&sort=activity&site=stackoverflow&filter=!9YdnSMKKT',
    {id: '@id'},
    {query: {method: "GET", isArray: false}
  });
});