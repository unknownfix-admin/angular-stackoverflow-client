angular.module('SearchApp').factory('userQuestions', 
  function ($resource, appConfig) {
    return $resource(appConfig.api_url + '2.2/users/:user_id/questions?page=1&pagesize=5&order=desc&sort=activity&site=stackoverflow',
      {user_id: '@user_id'},
      {query: {method: "GET", isArray: false}
    });
  }
);