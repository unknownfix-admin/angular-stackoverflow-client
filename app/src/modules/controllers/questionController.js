angular.module('SearchApp')
  .controller('QuestionCtrl', function ($scope, $routeParams, question, questionAnswers) {
  
    $scope.question = false;
    $scope.answers = [];
    
    question.query({id: $routeParams.question_id}).$promise.then(function (data) {
      $scope.question = data.items[0];
      questionAnswers.query({id: $routeParams.question_id}).$promise.then(function (data) {
        $scope.answers = data.items;
      });
    });
  });