angular.module('SearchApp')
  .controller('HomeCtrl', function ($scope, $location, search) {
      $scope.search_text = '';
      
      $scope.searchAction = function () {
        if ($scope.search_text.length === 0) {
          return false;
        }
  
        $location.path('/search/' + encodeURIComponent($scope.search_text));
      };
  });