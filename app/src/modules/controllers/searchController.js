angular.module('SearchApp')
  .controller('SearchCtrl', function ($scope, $timeout, $routeParams, $location, search, searchByTag, userQuestions) {
    
    $scope.search_text = $routeParams.intitle;
    $scope.items = [];
    
    $scope.sortType = 'activity';
    $scope.sortReverse = false;
  
    $scope.loader = true;
  
    $scope.quickQuestionTable = {
      show: false,
      loader: true,
      data: [],
      title: []
    };
    
    $scope.navigation = {
      td_count: 0,
      td: 0,
      elm: false
    };
    
    search.query({intitle: $routeParams.intitle}).$promise.then(function (data) {
      // $timeout что бы вы могли насладиться "загрузкой" данных
      $timeout(function () {
        $scope.loader = false;
        $scope.items = data.items;
      }, 1000);
    });
    
    $scope.onSortClick = function (field) {
      if ($scope.sortType !== field) {
        $scope.sortReverse = false;
      } else {
        $scope.sortReverse = !$scope.sortReverse;
      }
      
      $scope.sortType = field;
    };
    
    $scope.onAuthorClick = function (author) {
      $scope.quickQuestionTable.data = [];
      $scope.quickQuestionTable.loader = true;
      $scope.quickQuestionTable.show = true;
      $scope.quickQuestionTable.title = 'user ' + author.display_name;
  
      userQuestions.query({user_id: author.user_id}).$promise.then(function (data) {
        // $timeout что бы вы могли насладиться "загрузкой" данных
        $timeout(function () {
          $scope.quickQuestionTable.loader = false;
          $scope.quickQuestionTable.data = data.items;
        }, 1000);
      });
    };
  
    $scope.onTagClick = function (tag) {
      $scope.quickQuestionTable.data = [];
      $scope.quickQuestionTable.loader = true;
      $scope.quickQuestionTable.show = true;
      $scope.quickQuestionTable.title = 'tag ' + tag;
  
      searchByTag.query({tag: tag}).$promise.then(function (data) {
        // $timeout что бы вы могли насладиться "загрузкой" данных
        $timeout(function () {
          $scope.quickQuestionTable.loader = false;
          $scope.quickQuestionTable.data = data.items;
        }, 1000);
      });
    };
    
    $scope.onQuickQuestionTableClose = function () {
      $scope.quickQuestionTable.show = false;
    };
  
    $scope.questAction = function (quest_id) {
      $location.path('/question/' + quest_id);
    };
  });