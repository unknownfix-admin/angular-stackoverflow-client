'use strict';

angular.module('SearchApp').config(
  ['$routeProvider', '$locationProvider',
    function ($routeProvider, $locationProvider) {
      $locationProvider.hashPrefix('!');
      //$locationProvider.html5Mode(true);
      $routeProvider
        .when('/', {
          templateUrl: 'modules/views/home.html',
          controller: 'HomeCtrl'
        })
        .when('/search/:intitle', {
          templateUrl: 'modules/views/search.html',
          controller: 'SearchCtrl'
        })
        .when('/question/:question_id', {
          templateUrl: 'modules/views/question.html',
          controller: 'QuestionCtrl'
        })
        .otherwise({redirectTo: '/'});
    }
  ]
);