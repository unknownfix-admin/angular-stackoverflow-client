'use strict';

require('./vendor.js')();

angular
  .module('SearchApp', ['ngRoute', 'ngResource'])
  .config(function($provide) {
    $provide.provider('appConfig', function() {
      this.$get = function() {
        return {
          'api_url': 'http://api.stackexchange.com/'
        };
      };
    });
  });

require('./routes.js');

//Controllers
require('./modules/controllers/homeController.js');
require('./modules/controllers/searchController.js');
require('./modules/controllers/questionController.js');

//Services
require('./modules/services/searchService.js');
require('./modules/services/userService.js');
require('./modules/services/questionService.js');

//Filters
require('./modules/filters/trustFilter.js');

//Directives
require('./modules/directives/navigationDirective.js');