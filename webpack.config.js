'use strict';
var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var CopyWebpackPlugin = require('copy-webpack-plugin');

var APP = __dirname + '/app';

module.exports = {
  context: APP,
  entry: {
    app: ['./src/index.js']
  },
  output: {
    path: APP + '/dist',
    filename: "index.js"
  },
  module: {
    loaders: [
      {test: /\.css$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader")},
      {
        test: /\.(eot|woff|ttf|svg|png|jpg|gif)([\?]?.*)$/,
        loader: 'url-loader?limit=30000&name=media/[name]-[hash].[ext]'
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin("styles.css"),
    new CopyWebpackPlugin([
      {from: APP + '/src/index.html', to: APP + '/dist/index.html'},
      {from: APP + '/src/modules/views/', to: APP + '/dist/modules/views'},
      {from: APP + '/src/assets', to: APP + '/dist/assets'}
    ])
  ],
  loaders: [
    {
      test: /\.js$/,
      loader: 'ng-annotate!jshint',
      exclude: /node_modules/
    },
    {
      test: /\.html$/,
      loader: "raw-loader"
    }
  ]
};